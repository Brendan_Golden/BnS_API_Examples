function createNode(element) {
	return document.createElement(element);
}

function append(parent, el) {
	return parent.appendChild(el);
}

const TableName = document.getElementById('TableName');
const TablePrice = document.getElementById('TablePrice');
const TableQuantity = document.getElementById('TableQuantity');
const TableISO = document.getElementById('TableISO');
const urlIDLookup = "http://api.silveress.ie/bns/market";

function loadDropdown(){
	var dataList = document.getElementById('json-datalist');
	var input = document.getElementById('ajax');
	// Create a new XMLHttpRequest.
	var request = new XMLHttpRequest();
	// Handle state changes for the request.
	request.onreadystatechange = function(response) {
	  if (request.readyState === 4) {
		if (request.status === 200) {
		  // Parse the JSON
		  var jsonOptions = JSON.parse(request.responseText);

		  // Loop over the JSON array.
		  jsonOptions.forEach(function(item) {
			// Create a new <option> element.
			var option = document.createElement('option');
			// Set the value using the item in the JSON array.
			option.value = item.Name;
			// Add the <option> element to the <datalist>.
			dataList.appendChild(option);
		  });

		  // Update the placeholder text.
		  input.placeholder = "Search";
		} else {
		  // An error occured :(
		  input.placeholder = "Couldn't load datalist options :(";
		}
	  }
	};

	// Update the placeholder text.
	input.placeholder = "Loading options...";

	// Set up and make the request.
	request.open('GET', urlIDLookup, true);
	request.send();
}

function currencyConvert(number){
	var str = number;
	var str = str.toString()
	var len = str.length
	var gold =""
	var silver = ""
	var copper = ""
	if(len > 4){
		var gold = str.substring( 0 , len -4)+ "g";
	}
	if(len > 2){
		var silver = str.substring( len -2 ,len - 4 )+ "s";
	}
	if(len > 0){
		var copper = str.substring( len ,len -2 )+ "c";
	} 
	var total = gold  + silver  + copper; 
	return total;
}

function pull_data_controller(){
	document.getElementById("resultsTable").style.display = "table";
	document.getElementById("warning").style.display = "block";
	pull_data()
}

function pull_data(){
	var region = ""
	var id = "";
	var name = document.getElementById("ajax").value;
	var e = document.getElementById("regionSelect");
	var region = e.options[e.selectedIndex].value;
	var urlCurrent = 'http://api.silveress.ie/bns/market/current/' + region;
	
	fetch(urlIDLookup)
		.then((resp) => resp.json())
		.then(function(data) {
			return data.map(function(data2) {
				if(data2.Name == name){
					var id = data2.ID;						
					fetch(urlCurrent)
						.then((resp) => resp.json())
						.then(function(data) {
							return data.map(function(data2) {
								if(data2.ID == id){
									var date = new Date(data2.ISO);
									let span1 = createNode('span');
									let span2 = createNode('span');
									let span3 = createNode('span');
									let span4 = createNode('span');
									let span5 = createNode('span');
									
									
									while (TableName.firstChild) {
										TableName.removeChild(TableName.firstChild);
									}
									while (TablePrice.firstChild) {
										TablePrice.removeChild(TablePrice.firstChild);
									}
									while (TableQuantity.firstChild) {
										TableQuantity.removeChild(TableQuantity.firstChild);
									}
									while (TableISO.firstChild) {
										TableISO.removeChild(TableISO.firstChild);
									}
									
									span1.innerHTML = `${name}`;
									span3.innerHTML = `${currencyConvert(data2.ItemPrice)}`;
									span4.innerHTML = `${data2.Quantity}`;
									span5.innerHTML = `${date}`;
									
									append(TableName, span1);
									append(TablePrice, span3);
									append(TableQuantity, span4);
									append(TableISO, span5);
									
								}
							})
						})
						.catch(function(error) {
						console.log(error);
						});  
					const urlHistory = 'http://api.silveress.ie/bns/market/history/' + region + "/" + id;

					$(document).ready(function() {
						var options = {								
							rangeSelector: {
								selected: 1,
								inputEnabled: $('#container2').width() > 480
							},
							chart: {
								renderTo: 'container2'
							},
							title: {
								text: name
							},
							rangeSelector: {
								//allButtonsEnabled: true,
								selected: 0,
								buttons:[	
									{type: 'day', count: 1, text: '1d'},
									{type: 'week', count: 1, text: '1w'},
									{type: 'month', count: 1, text: '1m'},
									{type: 'month', count: 3, text: '3m'}, 
									{type: 'month', count: 6, text: '6m'}, 
									{type: 'year', count: 1, text: '1y'}, 
									{type: 'all', text: 'All'}
								]			
							},
							xAxis: {
								type: 'datetime',
								labels: {
									formatter: function () {
										return Highcharts.dateFormat('%a %d %b %H:%M', this.value);
									},
									dateTimeLabelFormats: {
										minute: '%H:%M',
										hour: '%H:%M',
										day: '%e. %b',
										week: '%e. %b',
										month: '%b \'%y',
										year: '%Y'
									}
								}
							},
							yAxis: [{ //--- Primary yAxis
								title: {
									text: 'Price'
								},
								labels: {
									formatter: function() {
										return currencyConvert(this.value);
									}
								},
								opposite: false,
								min: 0
							}, { //--- Secondary yAxis
								title: {
									text: 'Volume'
								},
								opposite: true,
								min: 0
							}],
									
							series: [{},{},{},{},{}],
							tooltip: {
								shared: true,
								useHTML: true,
								formatter: function () {
									var s = '<b style="text-align:centre">' + Highcharts.dateFormat('%b %e, %Y, %H:%M', this.x) + '</b>';
									s += "<table>"
									for(var i=0; i < this.points.length;i++){
										if(i < 3){						
											var total = currencyConvert(this.points[i].point.y)
											s += '<tr><td style="font-weight:bold; color:'+ this.points[i].point.color +'">' + this.points[i].point.series.name + ':</td><td style="text-align: right"> ' + total + '</td></tr>';
										}
										if(i >= 3 ){
											var str =this.points[i].point.y

											s += '<tr><td style="font-weight:bold; color:'+ this.points[i].point.color +'">' + this.points[i].point.series.name + ':</td><td style="text-align: right"> ' + str + '</td></tr>'; 
										}
									}
									s += "</table>"

									return s;
								}
							}
								
						};

						$.getJSON(urlHistory, function(data) {
							var date = new Array();
							var minPrice = new Array();
							var avgPrice = new Array();
							var maxPrice = new Array();
							var totalItems = new Array();
							var totalListings = new Array();
							
							var minPriceArray = new Array();
							var avgPriceArray = new Array();
							var maxPriceArray = new Array();
							var totalItemsArray = new Array();
							var totalListingsArray = new Array();
							
							
							for(var i=0; i < data.length;i++){	
								
								minPriceArray[0] = Date.parse(data[i].timeDate)
								minPriceArray[1] = data[i].minPrice
								minPrice[i] = minPriceArray

								avgPriceArray[0] = Date.parse(data[i].timeDate)
								avgPriceArray[1] = data[i].avgPrice
								avgPrice[i] = avgPriceArray
								
								maxPriceArray[0] = Date.parse(data[i].timeDate)
								maxPriceArray[1] = data[i].maxPrice
								maxPrice[i] = maxPriceArray
								
								totalItemsArray[0] = Date.parse(data[i].timeDate)
								totalItemsArray[1] = data[i].totalItems
								totalItems[i] = totalItemsArray
								
								totalListingsArray[0] = Date.parse(data[i].timeDate)
								totalListingsArray[1] = data[i].totalListings
								totalListings[i] = totalListingsArray
								
								
								minPriceArray = [];
								avgPriceArray = [];
								maxPriceArray = [];
								totalItemsArray = [];
								totalListingsArray = [];

							}
							
							options.series[0].data = minPrice;
							options.series[0].name = "Mininum Price";
							
							
							options.series[1].data = avgPrice;
							options.series[1].name = "Average Price";
							
							
							options.series[2].data = maxPrice;
							options.series[2].name = "Max Visible Price";
							
							
							options.series[3].data = totalItems;
							options.series[3].name = "Total visible Items";
							options.series[3].yAxis = 1;
							options.series[3].type = 'column';
							options.series[3].type.pointPadding = 0;
							options.series[3].type.borderWidth = 0;
							options.series[3].type.pointPadding = 0;
							options.series[3].type.shadow = false;
							
							
							options.series[4].data = totalListings;
							options.series[4].name = "Total Listings";
							options.series[4].yAxis = 1;
							options.series[4].type = 'column';
							options.series[4].type.pointPadding = 0;
							options.series[4].type.borderWidth = 0;
							options.series[4].type.pointPadding = 0;
							options.series[4].type.shadow = false;
							

							options.series[0].zIndex = 1;
							options.series[1].zIndex = 1;
							options.series[2].zIndex = 1;
							
							var chart = new Highcharts.StockChart(options);
						});

					});				
				}
			})
		})
		.catch(function(error) {
		console.log(error);
		});  
}
loadDropdown()